# Copyleft (c) January, 2022, Oromion.
# Usage: docker build -t dune-archiso/dune-archiso:dune-core .

FROM registry.gitlab.com/dune-archiso/images/dune-archiso

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune Archlinuxcn" \
  description="Dune in Archlinuxcn" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso-archlinuxcn/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso-archlinuxcn" \
  vendor="Oromion Aznarán" \
  version="1.0"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN curl -s https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/add_archlinuxcn.sh | bash && \
  sudo pacman -Syyuq --needed --noconfirm archlinuxcn-keyring && \
  sudo pacman -Scc <<< Y <<< Y && \
  sudo rm -r /var/lib/pacman/sync/*